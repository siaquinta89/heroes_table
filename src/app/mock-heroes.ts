import { Hero } from './hero';

export const HEROES: Hero[] = [
  {
    id: 1,
    nome: 'Mario',
    eta: 28,
    sesso: 'uomo',
    segni: true,
    note: 'non ci sono note'
  },
  {
    id: 2,
    nome: 'Giulia',
    eta: 34,
    sesso: 'donna',
    segni: false,
    note: 'non ci sono note'
  },
  {
    id: 3,
    nome: 'Luca',
    eta: 34,
    sesso: 'uomo',
    segni: false,
    note: 'non ci sono note'
  }
];
