import { Component, OnInit, Input } from '@angular/core';
import { HEROES } from '../mock-heroes';
import { Hero } from '../hero';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { ModalService } from '../modal/modal.service';

export interface DialogData {
  hero: Hero;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  heroes = HEROES;

  selectedHero: Hero;

  constructor(public dialog: MatDialog, private dialogService: ModalService) {}

  ngOnInit(): void {}

  openModal(hero: Hero): void {
  
    console.log('sto aprendo il modale', hero);

    funzionava
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px',
      data: hero
    });
    


    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
    // const dialogConfig = new MatDialogConfig();

    // The user can't close the dialog by clicking outside its body
    // dialogConfig.disableClose = true;
    // dialogConfig.id = 'modal-component';
    // dialogConfig.height = '350px';
    // dialogConfig.width = '600px';

    // const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }
}
