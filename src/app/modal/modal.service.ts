import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { take,map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModalService {


  constructor(private dialog: MatDialog) { }
  dialogRef: MatDialogRef<any>;

  public open(hero) {
    this.dialogRef = this.dialog.open(MatDialog, {
      data: hero
    });
  }

  public confirmed(): Observable<any>{
    return this.dialogRef.afterClosed().pipe(take(1), map(res =>
    {
      return res;
    }))
  }
}
