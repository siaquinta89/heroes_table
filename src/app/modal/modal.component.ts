import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Hero } from '../hero';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent implements OnInit {
  heroData: Hero;
  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Hero
  ) {
    this.heroData = this.data;
  }
  // @Input() hero: Hero;


  ngOnInit() {
    console.log('sono dentro il modale');
  }

  public confirm() {
    this.closeConfirm(this.heroData);
  }

  public closeConfirm(hero: Hero) {
    this.dialogRef.close(hero);
  }


  // If the user clicks the cancel button a.k.a. the go back button, then\
  // just close the modal
  closeModal() {
    this.dialogRef.close();
  }
}
