export interface Hero {
  id: number;
  nome: string;
  eta: number;
  sesso: string;
  segni: boolean;
  note: string;
}